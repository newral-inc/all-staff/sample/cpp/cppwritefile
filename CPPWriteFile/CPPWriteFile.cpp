﻿#include "pch.h"
#include <iostream>
#include <string>
#include <fstream>

int main()
{
	// ファイル名を指定します。
	// 絶対パスか相対パスで指定します。
	// ファイル名のみの場合、カレントディレクトリのファイルが対象となります。
	std::string fileName = "test.txt";

	// ファイルを開きます。
	std::ofstream fout;
	fout.open(fileName, std::ios::out);

	// ファイルが開けない場合
	if (!fout)
	{
		// エラーメッセージを表示して終了します。
		std::cout << "ファイルが開けません" << std::endl;
		return -1;
	}

	// ファイルにデータを書き込みます。
	fout << "1行目" << std::endl;
	fout << "2行目" << std::endl;
	fout << "2行目" << std::endl;

	// ファイルを閉じます。
	fout.close();
	return 0;
}
